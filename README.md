# 微控（microcontrol）

**微控**是一款为了方便使用安卓手机操作电脑而设计的助手类应用，配合微控电脑客户端，微控可以大大提升您的远程电脑操作体验。
 
- **功能丰富** ：模拟电脑鼠标、键盘以及各种游戏手柄，充分满足您的使用需求；
- **简洁高效** ：精简设计，简单易懂，只需几步您就能使用微控与电脑建立无缝连接；
- **深度整合** ：拥有微控电脑或者手机任一客户端您便可以方便的下载所有组件，无须繁杂过程。
- 点击下载桌面[电脑离线客户端][1]或者[手机安装包][2]；

![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/all_page.png)

-------------------


## 安卓端结构设计

> 本程序利用安卓手机搭载的蓝牙与传感器，实现对人体动作的采集与转化，通过蓝牙网络将数据传送至电脑端，电脑端程序控制电脑实现一系列体感操作。

在项目的初期阶段我们分析了现今电脑操作的需求，定义了本程序是一款方便用户无线操纵电脑的助手类软件，我们需要用手机远程控制电脑，无线操纵PPT演示过程以及模拟无线手柄等。确定了软件功能后，软件正式开发前期我们调研了流行安卓平台的手机的硬件配置，了解了目前安卓手机普遍拥有蓝牙3.0甚至4.0标准的外设，配有支持多点触控的屏幕，搭载有丰富的传感器等，足以满足我们这个项目手机端的开发需求。同时在PC平台，windows SDK拥有丰富的API可以实现对电脑的操作，同时功能强大的C#语言也足以方便的开发这样一款软件。前期的调查和研究表明本程序具有充分的可行性与必要性。

###1.软件需求调查
在项目的初期阶段我们分析了现今电脑操作的需求，定义了本程序是一款方便用户无线操纵电脑的助手类软件，我们需要用手机远程控制电脑，无线操纵PPT演示过程以及模拟无线手柄等。确定了软件功能后，软件正式开发前期我们调研了流行安卓平台的手机的硬件配置，了解了目前安卓手机普遍拥有蓝牙3.0甚至4.0标准的外设，配有支持多点触控的屏幕，搭载有丰富的传感器等，足以满足我们这个项目手机端的开发需求。同时在PC平台，windows SDK拥有丰富的API可以实现对电脑的操作，同时功能强大的C#语言也足以方便的开发这样一款软件。前期的调查和研究表明本程序具有充分的可行性与必要性。
###2.安卓端程序总体设计
安卓端负责体感数据的探测以及手势操作的接收，随后安卓端利用自带的蓝牙与电脑实施配对，借助串口通信将体感数据发送至电脑端解析。为了实现这个功能，经分析安卓端应大致具有以下几个模块：
####蓝牙配对连接模块
> 本模块处于底层驱动位置，负责安卓端蓝牙的扫描发现、接受外部设备的连接以及蓝牙信息的接受与发送等底层功能。

####全局通信连接模块
> 本模块负责全局所有界面层的消息调用需求，本模块应当提供上层应用对底层蓝牙状态的获取接口，对底层蓝牙扫描、断开等操作接口以及发送接收消息的接口。本模块承上启下，处于中层驱动的位置。

####界面层框架模块
> 本模块处于上层的界面部分，本模块应该搭建一个基本的界面框架，由于本程序在初期需求的不确定性，界面框架应该具有充分可扩展性以方便后期子组件的添加。

####子界面模块
> 程序中含有本模块若干，每个子界面负责一个具体的功能，本模块是程序具体功能实现的载体。

-------------------

### 指令格式
| 页面|指令|备注|
| :-------- |:--------| :--------: |
|全局|**up**|弹起|
|全局|**down**|按下|
|鼠标设置|**mouse_sensitivity**|  鼠标灵敏度   |
|鼠标使用|**left_button**|左键|
|鼠标使用|**right_button**|右键|
|鼠标使用|**touch**|触摸板动作|
|文档演示|**next_page**|下一页|
|文档演示|**last_page**|上一页|
|文档演示|**enter_ppt**|进入演示|
|文档演示|**exit_ppt**|退出演示|
|按键手柄|**keyboard_reset_btn**|复位键|
|按键手柄|**keyboard_select_btn**|选择键|
|按键手柄|**keyboard_pos_front**|方向前|
|按键手柄|**keyboard_pos_back**|方向后|
|按键手柄|**keyboard_pos_left**|方向左|
|按键手柄|**keyboard_pos_right**|方向右|
|按键手柄|**keyboard_pos_front_right**|右上|
|按键手柄|**keyboard_pos_right_back**|右下|
|按键手柄|**keyboard_pos_back_left**|左下|
|按键手柄|**keyboard_pos_left_front**|左上|
|按键手柄|**keyboard_mode_a**|a键|
|按键手柄|**keyboard_mode_b**|b键|
|按键手柄|**keyboard_mode_x**|x键|
|按键手柄|**keyboard_mode_y**|y键|

> **提示：**想了解更多，请查看**项目主页**[wecon-release][3]或者**开发文档**[doc][4]。

![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/3d_vision.png)
![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/three_layer.png)

### 项目目标

本项目将长期更新，后续版本将会加入更多功能

- [x] 鼠标灵敏度（两方均可以设置，互相更新即可）
- [x] PPT演示功能界面（自定义界面基础按键）
- [x] 音量键快捷模式
- [x] 体感功能（重力传感器＋界面基础按钮）
- [x] 手柄界面灵敏度
- [x] 自定义seekbar
- [x] 细项设置＋程序更新＋关于等等
- [x] 访问下载区
- [x] 显示项目使用文档
- [x] 检查更新
- [x] 引导页
- [ ]  多指模式
- [ ] 磁场感应模式
- [ ] 总体程序设置
- [ ] 一键控制电脑等功能
- [ ] 语音识别
- [ ] 水波纹特效
- [ ] 曲线图表
- [ ] 瀑布流
- [ ] 主界面连接状态动画
- [ ] 二维码扫描
- [ ] 换肤
- [ ] 雷达扫描蓝牙
- [ ] 社会化分享

> **Tips：**目前仅仅开发了**微控**的一部分功能，上述也只是部分计划中的功能扩展，如果您对微控有什么好的意见或者建议，请按照本页最下方的联系方式联系我们，帮助微控更好地成长，微控开发小组对您的支持表示万分感谢！


## 安卓端代码设计

### 蓝牙配对连接模块
安卓端为了实现上述的功能模块，我们仔细研究并重新编写了google官方提供的例程，深入理解了android平台对Bluetooth 3.0标准的操作过程，**本过程中的代码已经在oschina网站开源，[点击查看][5]。**
![android-BluetoothChat](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/blechat.png)
根据本例程原理，我们编写了BluetoothConnectService类放在com.tanxiaoyao.wecon.net包下，本类实现的蓝牙配对连接模块所需全部功能，并对外提供了调用接口。


### 全局通信连接模块
为了方便上层界面对底层蓝牙的操作，我们编写了GlobalConnectHelper类来提供统一的实现接口及调用方法。本类可以实现获取蓝牙状态、获取连接服务实例、设置蓝牙连接参数、扫描并连接周围设备等一系列蓝牙操作。

### 界面层框架模块
经过不断的比较，我们选定了TabHost组成的菜单界面作为页面的扩展框架，**ActivityMain**即为本结构的代码实现。
>**结构示意图：**蓝牙驱动和全局连接管理使用单例模式，为全局提供蓝牙调用接口
>![view frame](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/frame_pic.png)

### 子界面模块
子界面由一个个基本的Activity组成，经过设计编写，本程序具有如下几个子界面**ActivityHome、ActivityMouse、ActivityMouseMain、ActivityPowerPoint ActivitySimulate、ActivityHandle、ActivityGravityHandle、ActivitySensorTest、ActivityAbout**。分别为**主页、鼠标设置、鼠标操作、PPT操控、手柄选择、案件手柄操作、重力手柄操作、传感器检测以及关于程序界面**。

### 网络模块实现
在应用更新升级部分，首先将应用描述配置文档放在服务端，然后再安卓端利用http连接请求该文件，利用**XmlPullParser**解析请求到的xml字段，然后将解析到的信息存放在**UpdataInfo**类中，程序就可以判断是否有新版本并通过http下载新文件。程序的帮助信息以markdown形式存放，其中可以链接存放在git上的图片，手机通过第三方开源插件**android-markdown-view**加载markdown文件并从服务器上读取所需的图片。客户端下载按钮会发起一个Uri连接到下载页面供用户下载。

> **提示：**升级时若您遇到服务器error等非网络连接引起的错误，请及时联系下方的联系方式，我们会尽快做出处理。

### 最终界面效果

![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/flat_ui.png)
![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/with_mac.png)


## 使用帮助
### 蓝牙连接、鼠标与PPT控制
>如下图所示，按照一般情况下的使用顺序，首先进入主页请按照界面提示扫描并连接您安装有微控电脑端的设备，设备成功匹配后电脑和手机会同时显示连接成功，随后您可以选择进入任何一个功能界面。在鼠标设置界面，位于上方的使用提示显示了几种基本使用方法，同时您可以通过文本下方的滑动条设置鼠标在屏幕上的移动速度，点击开启触控板按钮您就可以像使用实体触控板那样使用微控。在文档演示界面，您可以借助微控操纵电脑上的ppt，界面中的按钮分别可以进入ppt全屏演示、下一步、上一步以及退出全屏演示。
![sum_pic2](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/sumpic2.png)


### 体感选择、相关信息
>如下图所示，进入体感界面，您可以使用您使用手柄的方式，分为传统方式与重力方式。按下菜单键，在弹出菜单中选择传感器，您会看到您设备上所有传感器的列表，若您没有加速度传感器，您可能无法正常使用重力手柄。在菜单中选择关于，您会进入到本程序的相关信息展示界面，这里展示了微控的基本信息，同时您可以检查更新，或者利用帮助查看程序内置的markdown帮助文件，帮助文档与项目主页的readme.md文件同步更新。您还可以直接使用客户端下载按钮进入项目的下载地址，可以下载微控的安卓端以及电脑端安装包。
![sum_pic3](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/sumpic3.png)

### 操作界面
>下图所示为微控使用的界面，您可在实际使用中体验其具体功能。
![sum_pic4](http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/res/sumpic4.png)


## 参考文献
>程序的开发过程中，我们参照了大量的开源作者的相关代码，包括使用了第三方的markdown编辑插件，我们十分感谢这些无私的作者对我们的帮助。由于种种原因，我们暂时不能对这个程序开源，但是如果您对本项目感兴趣或者您想了解其中某部分的细节，联系下面的地址，我们愿意尽自己的绵薄之力为开源社区努力。

>1.Theresa Neil(美) 移动应用UI设计模式 北京：人民邮电出版社，2013

>2.Reto Meier(美) Professional Android 2 Application Development,Wiley Publishing,Inc,2012

>3.[Google Android developers site](http://developer.android.com/index.html)

>4.[启动splash](http://blog.csdn.net/hknock/article/details/12161159)

>5.[底部Tab栏](http://blog.csdn.net/mimitracely/article/details/7968743)

>6.[传感器介绍](http://www.oschina.net/question/163910_28354)

>7.[Socket通信](http://www.cnblogs.com/harrisonpc/archive/2011/03/31/2001565.html)

>8.[蓝牙连接](http://www.2cto.com/kf/201210/161596.html)

>9.Google蓝牙聊天例程：Android Open Source Project BluetoothChat

>10.[开源项目Remote-control](http://git.oschina.net/lujianing/android-remote-control-computer)

>11.[安卓开源markdown编辑库](https://github.com/falnatsheh/MarkdownView)

>12.[应用升级更新参考](http://www.javaapk.com/article/demodownload)
 
## 联系方式

###安卓端
-Email    :<junlidev@gmail.com> 

-QQ    	:2561299250

-git		:[叹逍遥](http://git.oschina.net/tanxiaoyao)

---------
感谢您的阅读，欢迎您继续关注微控！

  [1]: http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/download/windows/wecon.exe
  [2]: http://git.oschina.net/tanxiaoyao/wecon-release/raw/master/download/android/wecon.apk
  [3]: http://git.oschina.net/tanxiaoyao/wecon-release
  [4]: http://git.oschina.net/tanxiaoyao/wecon-release/tree/master/doc
  [5]: http://git.oschina.net/tanxiaoyao/android-BluetoothChat

